package utils

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"io/ioutil"
	"path/filepath"
)

const (
	defaultInputDir  = "."
	defaultOutputDir = "output"
	defaultTmpDir    = "tmp"
	configFile       = "config.json"
)

type Config struct {
	TmpDir     string `json:"tmpDir"`
	OutputDir  string `json:"outputDir"`
	InputDir   string `json:"inputDir"`
	NumThreads int    `json:"numThreads"`
}

func NewConfigFromConfigFile(path string) *Config {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	c := Config{}
	if err := json.Unmarshal(file, &c); err != nil {
		panic(err)
	}

	return &c
}

func NewDefaultConfig() *Config {
	return &Config{
		TmpDir:     InPwd(defaultTmpDir),
		InputDir:   InPwd(defaultInputDir),
		OutputDir:  InPwd(defaultOutputDir),
		NumThreads: 4,
	}
}

func NewConfig() *Config {
	path, err := findConfigFile()
	if err != nil {
		fmt.Println("can't find a config file, will create a default one")
		c := NewDefaultConfig()
		c.Store()
		return c
	} else {
		return NewConfigFromConfigFile(path)
	}
}

func (c *Config) Store() {
	bs, err := json.Marshal(c)
	if err != nil {
		panic(err)
	}

	p := filepath.Join(Pwd(), configFile)
	if err := ioutil.WriteFile(p, bs, fs.ModePerm); err != nil {
		panic(err)
	}
}

func findConfigFile() (string, error) {
	pd := Exed()
	wd := Pwd()

	candidates := []string{
		filepath.Join(pd, configFile),
		filepath.Join(wd, configFile),
	}

	for _, c := range candidates {
		if Exists(c) {
			return c, nil
		}
	}

	return "", fmt.Errorf("can't find a config file anywhere")
}
