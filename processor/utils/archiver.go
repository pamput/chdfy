package utils

import (
	"github.com/mholt/archiver/v3"
	"path/filepath"
	"strings"
)

var archiveSupportedInput = map[string]interface{}{
	".zip": nil,
	".rar": nil,
	".tar": nil,
}

type ArchiveExtractor struct{}

func (s *ArchiveExtractor) ExtractArchive(inputFile string, outDirectory string) error {
	return archiver.Unarchive(inputFile, outDirectory)
}

func (c *ArchiveExtractor) IsSupportedInput(input string) bool {
	lowPath := strings.ToLower(input)
	ext := filepath.Ext(lowPath)
	_, ok := archiveSupportedInput[ext]
	return ok
}
