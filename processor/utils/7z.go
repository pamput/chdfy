package utils

import (
	"fmt"
	"github.com/kjk/lzmadec"
	"path/filepath"
	"strings"
)

var szSupportedInput = map[string]interface{}{
	".7z": nil,
}

type SzExtractor struct {}

func (s *SzExtractor) GetArchive(filePath string) (*lzmadec.Archive, error) {
	return lzmadec.NewArchive(filePath)
}

func (s *SzExtractor) ExtractArchive(a *lzmadec.Archive, outDirectory string) error {
	for _, e := range a.Entries {
		//fmt.Printf(" [7Z] Extracting: %s \n", e.Path)

		outPath := filepath.Join(outDirectory, e.Path)
		if Exists(outPath) {
			fmt.Printf(" [7Z] %s already exists \n", outPath)
		} else {
			if err := a.ExtractToFile(outPath, e.Path); err != nil {
				return err
			}
			//fmt.Printf(" [7Z] %s extracted!\n", e.Path)
		}
	}

	return nil
}

func (c *SzExtractor) IsSupportedInput(input string) bool {
	lowPath := strings.ToLower(input)
	ext := filepath.Ext(lowPath)
	_, ok := szSupportedInput[ext]
	return ok
}