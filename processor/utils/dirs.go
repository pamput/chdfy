package utils

import (
	"fmt"
	"github.com/kjk/lzmadec"
	"os"
	"path/filepath"
)

type Dirs struct {
	config *Config
}

func NewDirs(c *Config) Dirs {
	return Dirs{
		c,
	}
}

func (d *Dirs) InTmp(file ...string) string {
	files := []string{d.config.TmpDir}
	files = append(files, file...)
	return filepath.Join(files...)
}

func (d *Dirs) InOutput(file ...string) string {
	files := []string{d.config.OutputDir}
	files = append(files, file...)
	return filepath.Join(files...)
}

func (d *Dirs) InInput(file ...string) string {
	files := []string{d.config.InputDir}
	files = append(files, file...)
	return filepath.Join(files...)
}

func (d *Dirs) InitDirs() {
	os.Mkdir(d.config.TmpDir, 0755)
	os.Mkdir(d.config.OutputDir, 0755)
}

func (d *Dirs) CreateTempDir(g string) string {
	if err := os.Mkdir(d.InTmp(g), 0755); err != nil {
		fmt.Printf("WARNING: %+v", err)
	}
	return d.InTmp(g)
}

func (d *Dirs) CleanTempDir(g string) {
	if err := os.RemoveAll(d.InTmp(g)); err != nil {
		panic(err)
	}
}

func (d *Dirs) AlreadyProcessed7Z(a *lzmadec.Archive) bool {
	for _, e := range a.Entries {
		basename := filepath.Base(e.Path)[:len(e.Path)-len(filepath.Ext(e.Path))]
		if Exists(d.InOutput(basename + ".chd")) {
			return true
		}
	}
	return false
}

func (d *Dirs) AlreadyProcessedGame(game string) bool {
	return Exists(d.InOutput(game+".chd"))
}

func (d *Dirs) MarkGameAsError(game string, e error, msg string) {
	fmt.Printf(" [ERROR] %s: %s \n", game, msg)

	if Exists(d.InOutput(game+".chd.error")) {
		return
	}

	file, err := os.Create(d.InOutput(game+".chd.error"))
	defer file.Close()

	if err != nil {
		panic(err)
	}

	if msg != "" {
		if _, err := file.WriteString(msg+"\n\n"); err != nil {
			panic(err)
		}
	}

	if _, err := file.WriteString(e.Error()); err != nil {
		panic(err)
	}
}