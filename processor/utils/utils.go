package utils

import (
	"errors"
	"os"
	"path/filepath"
	"runtime"
)

func Pwd() string {
	pwd, _ := os.Getwd()
	return pwd
}

func Exed() string {
	ex, _ := os.Executable()
	pd := filepath.Dir(ex)
	return pd
}

func InPwd(file ...string) string {
	files := []string{Pwd()}
	files = append(files, file...)
	return filepath.Join(files...)
}

func Exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}

func numOfCores() int {
	return runtime.NumCPU()
}

func Basename(filename string) string {
	base := filepath.Base(filename)
	return filepath.Base(base)[:len(base)-len(filepath.Ext(base))]
}
