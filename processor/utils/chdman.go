package utils

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var chdmanSupportedInput = map[string]interface{}{
	".iso": nil,
	".cue": nil,
}

func ChdmanExec() string {
	ex, _ := os.Executable()
	pd := filepath.Dir(ex)
	wd, _ := os.Getwd()

	candidates := []string{
		filepath.Join(pd, "chdman.exe"),
		filepath.Join(pd, "chdman"),
		filepath.Join(wd, "chdman.exe"),
		filepath.Join(wd, "chdman"),
	}

	for _, c := range candidates {
		if Exists(c) {
			return c
		}
	}

	return "chdman"
}

type Chdman struct{}

func (c *Chdman) Createcd(input string, output string) error {
	chdman := exec.Command(
		ChdmanExec(),
		"createcd",
		"--force",
		"--input", input,
		"--output", output,
	)
	if err := chdman.Run(); err != nil {
		return err
	}

	return nil
}

func (c *Chdman) IsSupportedInput(input string) bool {
	lowPath := strings.ToLower(input)
	ext := filepath.Ext(lowPath)
	_, ok := chdmanSupportedInput[ext]
	return ok
}

func (c *Chdman) SCreatecd(input string, output string) (bool, error) {
	if !c.IsSupportedInput(input) {
		return false, nil
	}

	if !Exists(output) {
		c.Createcd(input, output+".processing")
		err := os.Rename(output+".processing", output)
		if err != nil {
			return false, err
		}
		return true, nil
	} else {
		fmt.Printf(" [CHDMAN] %s already exists\n", output)
		return false, nil
	}

}
