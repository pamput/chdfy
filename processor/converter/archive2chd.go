package converter

import (
	utils "7z2chd/processor/utils"
	"fmt"
	"io/ioutil"
)

func NewArchiveToChdConverter(config *utils.Config) *ArchiveToChdConverter {
	return &ArchiveToChdConverter{
		config: config,
		dirs:   utils.NewDirs(config),
		chdman: &utils.Chdman{},
		arc:     &utils.ArchiveExtractor{},
	}
}

type ArchiveToChdConverter struct {
	config *utils.Config
	dirs   utils.Dirs
	chdman *utils.Chdman
	arc    *utils.ArchiveExtractor
}

func (c *ArchiveToChdConverter) Convert(inputFile string) {
	if !c.IsValidInput(inputFile) {
		fmt.Printf("Skipping '%s', invalid format \n", inputFile)
		return
	}

	game := utils.Basename(inputFile)

	fmt.Println("Processing: " + game)

	c.dirs.CreateTempDir(game)
	defer c.dirs.CleanTempDir(game)

	if c.dirs.AlreadyProcessedGame(game) {
		fmt.Printf(" [SKIP] Skipping '%s' as '%s.chd' already found \n", game, game)
		c.dirs.CleanTempDir(game)
		return
	}

	if err := c.extractArchive(inputFile, game); err != nil {
		c.dirs.MarkGameAsError(game, err, "can't extract archive")
		return
	}
	if err := c.convertToChd(game); err != nil {
		c.dirs.MarkGameAsError(game, err, "can't convert to chd")
		return
	}

	fmt.Printf(" [DONE] %s \n", game)

}

func (c *ArchiveToChdConverter) extractArchive(inputFile string, game string) error {
	return c.arc.ExtractArchive(c.dirs.InInput(inputFile), c.dirs.InTmp(game))
}

func (c *ArchiveToChdConverter) convertToChd(game string) error {
	info, err := ioutil.ReadDir(c.dirs.InTmp(game))
	if err != nil {
		panic(err)
	}

	for _, i := range info {
		basename := utils.Basename(i.Name())
		if _, err := c.chdman.SCreatecd(
			c.dirs.InTmp(game, i.Name()),
			c.dirs.InOutput(basename+".chd"),
		); err != nil {
			return err
		}
	}
	return nil
}

func (c ArchiveToChdConverter) IsValidInput(input string) bool {
	return c.arc.IsSupportedInput(input)
}
