package converter

import (
	utils "7z2chd/processor/utils"
	"fmt"
	"github.com/kjk/lzmadec"
)

func NewSzToChdConverter(config *utils.Config) *SzToChdConverter {
	return &SzToChdConverter{
		config: config,
		dirs:   utils.NewDirs(config),
		chdman: &utils.Chdman{},
		sz:     &utils.SzExtractor{},
	}
}

type SzToChdConverter struct {
	config *utils.Config
	dirs   utils.Dirs
	chdman *utils.Chdman
	sz     *utils.SzExtractor
}

func (c *SzToChdConverter) Convert(inputFile string) {
	if !c.IsValidInput(inputFile) {
		fmt.Printf("Skipping '%s', invalid format \n", inputFile)
		return
	}

	game := utils.Basename(inputFile)

	fmt.Println("Processing: " + game)

	a, err := c.sz.GetArchive(c.dirs.InInput(inputFile))
	if err != nil {
		c.dirs.MarkGameAsError(game, err, "can't open archive")
		return
	}

	c.dirs.CreateTempDir(game)
	defer c.dirs.CleanTempDir(game)

	if c.dirs.AlreadyProcessed7Z(a) {
		fmt.Printf(" [SKIP] Skipping '%s' as '%s.chd' already found \n", game, game)
		c.dirs.CleanTempDir(game)
		return
	}

	if err := c.extractArchive(a, game); err != nil {
		c.dirs.MarkGameAsError(game, err, "can't extract archive")
		return
	}
	if err := c.convertToChd(a, game); err != nil {
		c.dirs.MarkGameAsError(game, err, "can't convert to chd")
		return
	}


	fmt.Printf(" [DONE] %s \n", game)

}

func (c *SzToChdConverter) extractArchive(a *lzmadec.Archive, game string) error {
	return c.sz.ExtractArchive(a, c.dirs.InTmp(game))
}

func (c *SzToChdConverter) convertToChd(a *lzmadec.Archive, game string) error {
	for _, e := range a.Entries {
		basename := utils.Basename(e.Path)
		_, err := c.chdman.SCreatecd(
			c.dirs.InTmp(game, e.Path),
			c.dirs.InOutput(basename+".chd"),
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c SzToChdConverter) IsValidInput(input string) bool {
	return c.sz.IsSupportedInput(input)
}
