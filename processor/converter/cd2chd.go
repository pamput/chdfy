package converter

import (
	utils "7z2chd/processor/utils"
	"fmt"
)

func NewCdToChdConverter(config *utils.Config) *CdToChdConverter {
	return &CdToChdConverter{
		config: config,
		dirs:   utils.NewDirs(config),
		chdman: &utils.Chdman{},
	}
}

type CdToChdConverter struct {
	config *utils.Config
	dirs   utils.Dirs
	chdman *utils.Chdman
}

func (c *CdToChdConverter) Convert(filename string) {
	if !c.IsValidInput(filename) {
		fmt.Printf("Skipping '%s', invalid format \n", filename)
		return
	}

	game := utils.Basename(filename)

	if c.dirs.AlreadyProcessedGame(game) {
		fmt.Printf(" [SKIP] Skipping '%s' as '%s.chd' already found \n", game, game)
		return
	}

	fmt.Println("Processing: " + game)

	if _, err := c.chdman.SCreatecd(
		c.dirs.InInput(filename),
		c.dirs.InOutput(game+".chd"),
	); err != nil {
		c.dirs.MarkGameAsError(game, err, "can't convert to chd")
		return
	}

	fmt.Printf(" [DONE] %s \n", game)
}

func (c CdToChdConverter) IsValidInput(input string) bool {
	return c.chdman.IsSupportedInput(input)
}
