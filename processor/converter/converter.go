package converter

import (
	"7z2chd/processor/utils"
	"os"
	"path/filepath"
)

type Converter interface {
	Convert(filename string)
}

func IsSupported(filename string) bool {
	return SzToChdConverter{}.IsValidInput(filename) ||
		CdToChdConverter{}.IsValidInput(filename) ||
		ArchiveToChdConverter{}.IsValidInput(filename)
}

func NewConverter(filename string, config *utils.Config) Converter {
	isSz := SzToChdConverter{}.IsValidInput(filename)
	isCd := CdToChdConverter{}.IsValidInput(filename)
	isAr := ArchiveToChdConverter{}.IsValidInput(filename)
	if isSz {
		return NewSzToChdConverter(config)
	} else if isCd {
		return NewCdToChdConverter(config)
	} else if isAr {
		return NewArchiveToChdConverter(config)
	} else {
		return nil
	}
}

func GetAllSupported(d utils.Dirs) []string {

	var result []string

	if err := filepath.Walk(d.InInput(), func(path string, info os.FileInfo, err error) error {
		if info.IsDir() || !IsSupported(info.Name()) {
			return nil
		}

		rel, err := filepath.Rel(d.InInput(), path)
		if err != nil {
			panic(err)
		}

		result = append(result, rel)
		return nil
	}); err != nil {
		panic(err)
	}

	return result
}
