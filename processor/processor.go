package processor

import (
	"7z2chd/processor/converter"
	"7z2chd/processor/utils"
	"fmt"
	"os"
	"sync"
)

type Processor struct {
	config *utils.Config
	dirs   utils.Dirs
}

func NewProcessor(c *utils.Config) Processor {
	return Processor{
		config: c,
		dirs:   utils.NewDirs(c),
	}
}

func (p *Processor) Run() {
	p.dirs.InitDirs()
	fmt.Printf("CHDMAN: %s \n", utils.ChdmanExec())
	fmt.Printf("THREADS: %d \n", p.config.NumThreads)

	toProcessChan := make(chan string, 50)

	var wg sync.WaitGroup
	wg.Add(p.config.NumThreads)

	for i := 0; i < p.config.NumThreads; i++ {
		go p.process(&wg, toProcessChan)
	}

	for _, f := range converter.GetAllSupported(p.dirs) {
		toProcessChan <- f
	}
	close(toProcessChan)

	wg.Wait()

	fmt.Println("-- Fin --")

	os.Exit(0)
}

func (p *Processor) process(wg *sync.WaitGroup, input chan string) {
	for i := range input {
		c := converter.NewConverter(i, p.config)
		c.Convert(i)
	}
	wg.Done()

}
