package main

import (
	"7z2chd/processor"
	"7z2chd/processor/utils"
)

func main() {
	c := utils.NewConfig()
	p := processor.NewProcessor(c)
	p.Run()
}
