module 7z2chd

go 1.17

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/dsnet/compress v0.0.2-0.20210315054119-f66993602bf5 // indirect
	github.com/golang/snappy v0.0.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/kjk/lzmadec v0.0.0-20210713164611-19ac3ee91a71 // indirect
	github.com/klauspost/compress v1.11.4 // indirect
	github.com/klauspost/pgzip v1.2.5 // indirect
	github.com/mholt/archiver/v3 v3.5.1 // indirect
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.2 // indirect
	github.com/ulikunitz/xz v0.5.9 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
)
